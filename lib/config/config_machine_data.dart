import 'package:app_coin_wash_manager/model/machine.dart';

final List<Machine> configMachineMokup = [
  Machine(1, '[세탁기]세탁기1', '2022-10-01'),
  Machine(2, '[세탁기]세탁기2', '2022-10-01'),
  Machine(3, '[세탁기]세탁기3', '2022-10-01'),
  Machine(4, '[세탁기]세탁기4', '2022-10-01'),
  Machine(5, '[세탁기]세탁기5', '2022-10-01'),
  Machine(6, '[건조기]건조기1', '2022-10-01'),
  Machine(7, '[건조기]건조기2', '2022-10-01'),
  Machine(8, '[건조기]건조기3', '2022-10-01'),
  Machine(9, '[건조기]건조기4', '2022-10-01'),
  Machine(10, '[건조기]건조기5', '2022-10-01'),
];