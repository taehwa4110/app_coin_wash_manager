import 'package:flutter/material.dart';

class ComponentAppbarActions extends StatefulWidget with PreferredSizeWidget {
  const ComponentAppbarActions({
    super.key,
    required this.title,
    this.isUseActionBtn1 = false,
    this.action1Icon,
    this.action1Callback,
    this.isUseActionBtn2 = false,
    this.action2Icon,
    this.action2Callback,
  });

  final String title;
  final bool isUseActionBtn1;
  final IconData? action1Icon;
  final VoidCallback? action1Callback;
  final bool isUseActionBtn2;
  final IconData? action2Icon;
  final VoidCallback? action2Callback;

  @override
  State<ComponentAppbarActions> createState() => _ComponentAppbarActionsState();

  @override
  Size get preferredSize {
    return const Size.fromHeight(45);
  }
}

class _ComponentAppbarActionsState extends State<ComponentAppbarActions> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: false,
      automaticallyImplyLeading: false,
      title: Text(widget.title),
      elevation: 1.0,
      actions: [
        if (widget.isUseActionBtn1) 
          IconButton(onPressed: widget.action1Callback, icon: Icon(widget.action1Icon)),
        if (widget.isUseActionBtn2)
          IconButton(onPressed: widget.action2Callback, icon: Icon(widget.action2Icon)),
      ],
    );
  }
}
