import 'package:app_coin_wash_manager/config/config_color.dart';
import 'package:app_coin_wash_manager/model/machine_item.dart';
import 'package:flutter/material.dart';

class ComponentMachineItem extends StatelessWidget {
  const ComponentMachineItem({super.key, required this.machineItem, required this.callback});

  final MachineItem machineItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: _buildBody()
    );
  }

  Widget _buildBody() {
    if (machineItem.isUsed == '사용') {
      return Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: Colors.blue,
            borderRadius: BorderRadius.circular(30),
            border: Border.all(
                color: Colors.brown,
                style: BorderStyle.solid,
                width: 5
            )

        ),
        child: Row(
          children: [
            Container(
                width: 25,height: 25,
                decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.white, ),
                child: Center(child: Text(machineItem.id.toString()),)
            ),
            SizedBox(width: 20,),
            Column(
              children: [
                Text(machineItem.machineFullName),
                Text(machineItem.datePurchase),
              ],
            ),
            SizedBox(width: 80,),
            Text(machineItem.isUsed, style: TextStyle(fontWeight: FontWeight.bold),),
          ],
        ),
      );
    }else {
      return Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: Colors.redAccent,
            borderRadius: BorderRadius.circular(30),
            border: Border.all(
                color: Colors.brown,
                style: BorderStyle.solid,
                width: 5
            )

        ),
        child: Row(
          children: [
            Container(
                width: 25,height: 25,
                decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.white, ),
                child: Center(child: Text(machineItem.id.toString()),)
            ),
            SizedBox(width: 20,),
            Column(
              children: [
                Text(machineItem.machineFullName),
                Text(machineItem.datePurchase),
              ],
            ),
            SizedBox(width: 80,),
            Text(machineItem.isUsed, style: TextStyle(fontWeight: FontWeight.bold),),
          ],
        ),
      );
    }
  }
}
