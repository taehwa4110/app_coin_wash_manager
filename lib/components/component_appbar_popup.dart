import 'package:flutter/material.dart';

class ComponentAppbarPopup extends StatelessWidget with PreferredSizeWidget {
  const ComponentAppbarPopup({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: false,
      automaticallyImplyLeading: false,
      iconTheme: const IconThemeData( //아이콘 색깔변경
        color: Colors.black,
      ),
      backgroundColor: Colors.white,
      title: Text(
        title,
        style: TextStyle(
          color: Colors.black,
          fontSize: 16,
        ),
      ),
      elevation: 1,
      actions: [
        IconButton(
            onPressed:() => Navigator.of(context).pop(),
            icon: const Icon(Icons.clear)
        ),
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(40);
}
