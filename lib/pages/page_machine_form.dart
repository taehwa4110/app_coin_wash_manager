import 'package:app_coin_wash_manager/components/component_appbar_popup.dart';
import 'package:app_coin_wash_manager/components/component_custom_loading.dart';
import 'package:app_coin_wash_manager/components/component_notification.dart';
import 'package:app_coin_wash_manager/config/config_dropdown.dart';
import 'package:app_coin_wash_manager/config/config_form_validator.dart';
import 'package:app_coin_wash_manager/model/machine_detail.dart';
import 'package:app_coin_wash_manager/model/machine_name_update_request.dart';
import 'package:app_coin_wash_manager/model/machine_request.dart';
import 'package:app_coin_wash_manager/repository/repo_machine.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';

class PageMachineForm extends StatefulWidget {
  PageMachineForm({super.key, this.machineDetail});

  MachineDetail? machineDetail;

  @override
  State<PageMachineForm> createState() => _PageMachineFormState();
}

class _PageMachineFormState extends State<PageMachineForm> {
  final _formKey = GlobalKey<FormBuilderState>();


  Future<void> _putMachine(int id, MachineNameUpdateRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    
    await RepoMachine().putData(id, request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
          success: true,
          title: '기계 정보 수정 성공',
          subTitle: res.msg,
      ).call();
      
      Navigator.pop(
        context,
        [true]
      );
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
          success: false,
          title: '기계 정보 수정 실패',
          subTitle: '기계 정보 수정에 실패하였습니다'
      ).call();
    });
  }

  Future<void>_setMachine(MachineRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMachine().setData(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
          success: true,
          title: '기계 등록 성공',
          subTitle: res.msg
      ).call();

      Navigator.pop(
        context,
        [true]
      );
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
          success: false,
          title: '기계 등록 실패',
          subTitle: '기계 등록에 실패하였습니다'
      ).call();
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: '기계 ${widget.machineDetail == null ? '등록' : '수정'}'),
      body: _buildBody(),
      bottomNavigationBar: BottomAppBar(
        child: OutlinedButton(
          onPressed: () {
            if (_formKey.currentState?.saveAndValidate()??false) {
              if (widget.machineDetail == null) {
                MachineRequest request = MachineRequest(
                  _formKey.currentState!.fields['machineName']!.value,
                  _formKey.currentState!.fields['machineType']!.value,
                  double.parse(_formKey.currentState!.fields['machinePrice']!.value),
                  DateFormat('yyyy-MM-dd').format(_formKey.currentState!.fields['datePurchase']!.value)
                );

                _setMachine(request);
              } else {
                MachineNameUpdateRequest request = MachineNameUpdateRequest(
                  _formKey.currentState!.fields['machineName']!.value,
                );

                _putMachine(widget.machineDetail!.id, request);
              }
            }
          },
          child: Text(widget.machineDetail == null ? '등록' : '수정'),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: FormBuilder(
        key: _formKey,
        autovalidateMode: AutovalidateMode.disabled,
        initialValue: widget.machineDetail == null ? {} : {'machineName' : widget.machineDetail!.machineName,},
        child: Column(
          children: [
            FormBuilderTextField(
              name: 'machineName',
              decoration: const InputDecoration(
                labelText: '기계명',
              ),
              validator: FormBuilderValidators.compose([
                FormBuilderValidators.required(errorText: formErrorRequired),
                FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)),
                FormBuilderValidators.maxLength(15, errorText: formErrorMaxLength(15)),
              ]),
              keyboardType: TextInputType.text,
            ),

            if (widget.machineDetail == null)
              FormBuilderTextField(
                  name: 'machinePrice',
                decoration: const InputDecoration(
                  labelText: '기계 가격'
                ),
                readOnly: widget.machineDetail == null ? false : true,
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.numeric(errorText: formErrorNumeric),
                ]),
                keyboardType: TextInputType.number,
              ),

            if (widget.machineDetail == null)
              FormBuilderDropdown<String>(
                  name: 'machineType',
                  decoration: const InputDecoration(
                    labelText: '기계 타입'
                  ),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(errorText: formErrorRequired),
                  ]),
                  items: dropdownMachineType,
              ),

            if (widget.machineDetail == null)
              FormBuilderDateTimePicker(
                name: 'datePurchase',
                format: DateFormat('yyyy-MM-dd'),
                inputType: InputType.date,
                decoration: InputDecoration(
                  labelText: '구매일',
                  suffixIcon: IconButton(
                    icon: const Icon(Icons.close),
                    onPressed: () {
                      _formKey.currentState!.fields['datePurchase']?.didChange(null);
                    },
                  ),
                ),
                locale: const Locale.fromSubtags(languageCode: 'ko'),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                ]),
              ),

            if (widget.machineDetail != null)
              Text('기계가격 : ${widget.machineDetail!.machinePrice} '),
            if (widget.machineDetail != null)
              Text('기계타입 : ${widget.machineDetail!.machineTypeName} '),
            if (widget.machineDetail != null)
              Text('구매일 : ${widget.machineDetail!.datePurchase} '),
          ],
        ),
      ),

    );
  }
}
