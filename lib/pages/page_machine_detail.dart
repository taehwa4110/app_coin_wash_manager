import 'package:app_coin_wash_manager/components/component_appbar_popup.dart';
import 'package:app_coin_wash_manager/components/component_custom_loading.dart';
import 'package:app_coin_wash_manager/components/component_no_contents.dart';
import 'package:app_coin_wash_manager/components/component_notification.dart';
import 'package:app_coin_wash_manager/model/machine_detail.dart';
import 'package:app_coin_wash_manager/pages/page_machine_form.dart';
import 'package:app_coin_wash_manager/pages/page_main_index.dart';
import 'package:app_coin_wash_manager/repository/repo_machine.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PageMachineDetail extends StatefulWidget {
  const PageMachineDetail({super.key, required this.machineId});

  final int machineId;

  @override
  State<PageMachineDetail> createState() => _PageMachineDetailState();
}

class _PageMachineDetailState extends State<PageMachineDetail> {

  final NumberFormat numberFormat = NumberFormat('#,###');

  MachineDetail _detail = MachineDetail(0,'','',0,'');

  void initState() {
    super.initState();
    _getDetail();
  }

  Future<void> _delData() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    
    await RepoMachine().delData(widget.machineId).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
          success: true,
          title: '데이터 삭제 성공',
          subTitle: res.msg
      ).call();
      
      Navigator.pop(context);
      Navigator.pop(context, [true]);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
          success: false,
          title: '데이터 삭제 실패',
          subTitle: '데이터 삭제에 실패하였습니다'
      ).call();

      Navigator.pop(context);
    });
  }

  Future<void> _getDetail() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMachine().getDetail(widget.machineId).then((res){
      BotToast.closeAllLoading();


      setState(() {
        _detail = res.data;
        
        print(_detail);
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(
        title: '기계 상세정보',
      ),
      body: _buildBody(),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => PageMainIndex()),
                  );
                },
                icon: const Icon(Icons.laptop)
            ),
            IconButton(
                onPressed: () {},
                icon: const Icon(Icons.group_add)
            ),
            IconButton(
                onPressed: () {},
                icon: const Icon(Icons.event_note)
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBody() {
    if (_detail.id != 0) {
      return ListView(
        padding: EdgeInsets.only(left: 10, right: 10),
        children: [
          Text('고유번호 : ${_detail.id}'),
          SizedBox(height: 5,),
          Text('기계명 : ${_detail.machineName}'),
          SizedBox(height: 5,),
          Text('기계타입 : ${_detail.machineTypeName}'),
          SizedBox(height: 5,),
          Text('기계가격 : ${_detail.machinePrice}'),
          SizedBox(height: 5,),
          Text('구입날짜 : ${_detail.datePurchase}'),
          SizedBox(height: 15,),
          Container(
            child: Row(
              children: [
                OutlinedButton(
                    onPressed: () async {
                      final popup = await Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => PageMachineForm(machineDetail: _detail,))
                      );

                      if (popup != null && popup[0]) {
                        Navigator.pop(
                          context,
                          [true]
                        );
                      }
                    },
                    child: Text("수정하기")
                ),
                SizedBox(width: 8,),
                OutlinedButton(
                    onPressed: () {
                      _showDeleteDialog();
                    },
                    child: Text("삭제하기")
                ),
              ],
            ),
          )
        ],
      );
    }else {
      return SizedBox(
        height: MediaQuery.of(context).size.height - 45,
        child: const ComponentsNoContents(icon: Icons.not_interested, msg: '데이터가 없습니다'),
      );
    }
  }

  void _showDeleteDialog() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('기계정보 삭제'),
            content: const Text('정말 이 정보를 삭제하시겠습니까?'),
            actions: [
              OutlinedButton(
                onPressed: () {
                  _delData();
                },
                child: const Text('확인')
              ),
              OutlinedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('취소')
              ),
            ],
          );
        }
    );
  }
}
