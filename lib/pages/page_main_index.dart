import 'package:app_coin_wash_manager/components/component_appbar_actions.dart';
import 'package:app_coin_wash_manager/components/component_count_title.dart';
import 'package:app_coin_wash_manager/components/component_custom_loading.dart';
import 'package:app_coin_wash_manager/components/component_machine_item.dart';
import 'package:app_coin_wash_manager/components/component_no_contents.dart';
import 'package:app_coin_wash_manager/components/component_notification.dart';
import 'package:app_coin_wash_manager/model/machine_item.dart';
import 'package:app_coin_wash_manager/pages/page_machine_detail.dart';
import 'package:app_coin_wash_manager/pages/page_machine_form.dart';
import 'package:app_coin_wash_manager/repository/repo_machine.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageMainIndex extends StatefulWidget {
  const PageMainIndex({Key? key}) : super(key: key);

  @override
  State<PageMainIndex> createState() => _PageMainIndexState();
}

class _PageMainIndexState extends State<PageMainIndex> {
  final _scrollController = ScrollController();

  List<MachineItem> _list = [];
  int _totalItemCount = 0;

  @override
  void initState() {
    super.initState();
    _getList();
  }

  Future<void> _getList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMachine()
        .getList()
        .then((res) {
          BotToast.closeAllLoading();

          setState(() {
            _list = res.list;
            _totalItemCount = res.totalItemCount;
          });
    })
        .catchError((err) {
          BotToast.closeAllLoading();

          ComponentNotification(
            success: false,
            title: '데이터 로딩 실패',
            subTitle: '데이터 로딩에 실패하였습니다.',
          ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarActions(
        title: '기계 리스트',
        isUseActionBtn2: true,
        action2Icon: Icons.refresh,
        action2Callback: () {
          _getList();
        },
      ),
      body:ListView(
        controller: _scrollController,
        children: [
          Container(
            margin: EdgeInsets.only(left: 10, right: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('기계 리스트', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                OutlinedButton(
                    onPressed: () async {
                      final popup = await Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => PageMachineForm())
                      );

                      if (popup != null && popup[0]) {
                        _getList();
                      }
                    },
                    child: Text('등록하기')
                )
              ],
            ),
          ),
          ComponentCountTitle(icon: Icons.list, count: _list.length, unitName: '대', itemName: '기계'),
          _buildList()
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => PageMainIndex()),
                  );
                },
                icon: Icon(Icons.laptop)
            ),
            IconButton(
                onPressed: () {},
                icon: Icon(Icons.group_add)
            ),
            IconButton(
                onPressed: () {},
                icon: Icon(Icons.event_note)
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildList() {
    if (_totalItemCount > 0) {
      return Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ListView.builder(
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: _list.length,
                itemBuilder: (_, index) => ComponentMachineItem(
                    machineItem: _list[index],
                    callback: () async {
                      final popup = await Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => PageMachineDetail(machineId: _list[index].id)),
                      );

                      if (popup != null && popup[0]) {
                        _getList();
                      }
                    }
                )
            )
          ]
      );
    } else {
      return SizedBox(
        height: MediaQuery.of(context).size.height - 45,
        child: const ComponentsNoContents(icon: Icons.not_interested, msg: '데이터가 없습니다'),
      );
    }

  }
}

