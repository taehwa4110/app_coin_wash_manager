class MachineRequest {
  String machineName;
  String machineType;
  double machinePrice;
  String datePurchase;

  MachineRequest(this.machineName, this.machineType, this.machinePrice, this.datePurchase);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['machineName'] = this.machineName;
    data['machineType'] = this.machineType;
    data['machinePrice'] = this.machinePrice;
    data['datePurchase'] = this.datePurchase;

    return data;
  }
}