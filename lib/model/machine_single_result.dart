import 'package:app_coin_wash_manager/model/machine_detail.dart';

class MachineSingleResult {
  bool isSuccess;
  int code;
  String msg;
  MachineDetail data;

  MachineSingleResult(this.isSuccess, this.code, this.msg, this.data);

  factory MachineSingleResult.fromJson(Map<String, dynamic> json) {
    return MachineSingleResult(
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
      MachineDetail.fromJson(json['data']),
    );
  }
}