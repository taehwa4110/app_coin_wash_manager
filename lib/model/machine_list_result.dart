import 'package:app_coin_wash_manager/model/machine_item.dart';

class MachineListResult {
  List<MachineItem> list;
  int totalPage;
  int totalItemCount;
  int currentPage;
  bool isSuccess;
  int code;
  String msg;

  MachineListResult(this.list, this.totalPage, this.totalItemCount, this.currentPage, this.isSuccess, this.code, this.msg);

  factory MachineListResult.fromJson(Map<String, dynamic> json) {
    return MachineListResult(
      json['list'] != null ? (json['list'] as List).map((e) => MachineItem.fromJson(e)).toList() : [],
      json['totalPage'],
      json['totalItemCount'],
      json['currentPage'],
      json['isSuccess'],
      json['code'],
      json['msg'],
    );
  }

}