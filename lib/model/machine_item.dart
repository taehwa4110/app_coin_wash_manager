class MachineItem {
  int id;
  String machineFullName;
  String datePurchase;
  String isUsed;

  MachineItem(this.id, this.machineFullName, this.datePurchase, this.isUsed);

  factory MachineItem.fromJson(Map<String, dynamic>  json) {
    return MachineItem(
      json['id'],
      json['machineFullName'],
      json['datePurchase'],
      json['isUsed'],
    );
  }
}