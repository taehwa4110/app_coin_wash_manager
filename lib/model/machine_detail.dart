class MachineDetail {
  int id;
  String machineName;
  String machineTypeName;
  double machinePrice;
  String datePurchase;

  MachineDetail(this.id, this.machineName, this.machineTypeName, this.machinePrice, this.datePurchase);

  factory MachineDetail.fromJson(Map<String, dynamic> json) {
    return MachineDetail(
      json['id'],
      json['machineName'],
      json['machineTypeName'],
      json['machinePrice'],
      json['datePurchase'],
    );
  }
}