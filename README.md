### 코인빨래방 관리 APP
***
(개인프로젝트) 코인 빨래방을 관리하는 APP
***

### Language
```
Flutter 3.3.2
Dart 2.18.1
```

### 기능
```
*기계관리
 - 기계 등록
 - 기계 리스트 조회
 - 기계 상세정보 조회
 - 기계 이름 수정
 - 기계 정보 삭제

* 회원관리(미구현)

* 이용내역관리(미구현)
```

### 앱화면
>* 기계 리스트 화면
>
>![app_main](./images/app-main.png)


>* 기계 등록 화면
>
>![app_machine_register](./images/app-machine-register.png)


>* 기계 상세정보 화면
>
>![app_info-detail](./images/app-info-detail.png)


>* 기계 정보 수정 화면
>
>![app_machine-name-change](./images/app-machine-name-change.png)
